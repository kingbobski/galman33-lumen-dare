package  
{
	/**
	 * ...
	 * @author galman33
	 */
	public class Levels 
	{
		
		[Embed(source="res/levels/Level1.oel", mimeType="application/octet-stream")]
		private static const LEVEL1 :Class;
		
		[Embed(source="res/levels/Level2.oel", mimeType="application/octet-stream")]
		private static const LEVEL2 :Class;
		
		[Embed(source="res/levels/Level3.oel", mimeType="application/octet-stream")]
		private static const LEVEL3 :Class;
		
		[Embed(source="res/levels/Level4.oel", mimeType="application/octet-stream")]
		private static const LEVEL4 :Class;
		
		[Embed(source="res/levels/Level5.oel", mimeType="application/octet-stream")]
		private static const LEVEL5 :Class;
		
		[Embed(source="res/levels/Level6.oel", mimeType="application/octet-stream")]
		private static const LEVEL6 :Class;
		
		[Embed(source="res/levels/Level7.oel", mimeType="application/octet-stream")]
		private static const LEVEL7 :Class;
		
		[Embed(source="res/levels/Level8.oel", mimeType="application/octet-stream")]
		private static const LEVEL8 :Class;
		
		public static const LEVELS :Array = [LEVEL1, LEVEL2, LEVEL3, LEVEL4, LEVEL5, LEVEL6, LEVEL7, LEVEL8];
		
	}

}