package  
{
	import flash.geom.Rectangle;
	import flash.utils.getTimer;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Canvas;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.graphics.Text;
	
	/**
	 * ...
	 * @author galman33
	 */
	public class Gui extends Entity 
	{
		
		[Embed(source = "res/Light.png")]
		private static const LightGraphic :Class;
		
		[Embed(source = "res/LightBar.png")]
		private static const LightBarGraphic :Class;
		
		private var lightBarFillCanvas :Canvas;
		private var fillRect :Rectangle = new Rectangle();
		
		private var timeLeft :Text;
		
		private var titleText :Text;
		
		private var lightSprite :Spritemap
		
		public function Gui() 
		{
			super();
			layer = 0;
			lightSprite = new Spritemap(LightGraphic, 11, 18);
			lightSprite.scale = 2;
			lightSprite.y = 10;
			lightSprite.frame = 0;
			
			var lightBarImage :Image = new Image(LightBarGraphic);
			lightBarImage.scale = 4;
			lightBarImage.x = (FP.width - lightBarImage.scaledWidth) / 2;
			lightBarImage.y = 10;
			
			lightBarFillCanvas = new Canvas(lightBarImage.scaledWidth - 6 * GameWorld.SCALE, lightBarImage.scaledHeight - 6 * GameWorld.SCALE);
			lightBarFillCanvas.x = lightBarImage.x + 3 * GameWorld.SCALE;
			lightBarFillCanvas.y = lightBarImage.y  + 3 * GameWorld.SCALE;
			
			lightSprite.x = lightBarImage.x - lightSprite.scaledWidth - 10;
			
			timeLeft = new Text("10.0");
			timeLeft.size = 16;
			timeLeft.color = 0xFF0080;
			timeLeft.x = (FP.width - timeLeft.scaledWidth) / 2;
			timeLeft.y = 21;
			
			titleText = new Text("");
			titleText.width = FP.width;
			titleText.align = "center";
			titleText.y = 50;
			
			addGraphic(lightSprite);
			addGraphic(lightBarFillCanvas);
			addGraphic(lightBarImage);
			addGraphic(timeLeft);
			addGraphic(titleText);
			
			graphic.scrollX = graphic.scrollY = 0;
		}
		
		override public function update():void 
		{
			super.update();
			fillRect.x = 0;
			fillRect.y = 0;
			fillRect.height = lightBarFillCanvas.height;
			fillRect.width = lightBarFillCanvas.width;
			lightBarFillCanvas.fill(fillRect, 0, 0);
			fillRect.width = lightBarFillCanvas.width * (world as GameWorld).getLightLeftPercent();
			lightBarFillCanvas.fill(fillRect, 0xFFFF00, 1);
			
			timeLeft.text = (world as GameWorld).getLightLeft().toFixed(1);
			timeLeft.x = (FP.width - timeLeft.scaledWidth) / 2;
			
			if ((world as GameWorld).getLightLeft() <= 0)
				lightSprite.frame = 1;
		}
		
		public function setTitleText(title :String) :void
		{
			titleText.text = title;
		}
		
	}

}