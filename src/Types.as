package  
{
	/**
	 * ...
	 * @author galman33
	 */
	public class Types 
	{
		
		public static const TYPE_MAZE :String = "TYPE_MAZE";
		public static const TYPE_END_TORCH :String = "TYPE_END_TORCH";
		public static const TYPE_BRIDGE :String = "TYPE_BRIDGE";
		public static const TYPE_BUTTON :String = "TYPE_BUTTON";
		public static const TYPE_PLAYER :String = "TYPE_PLAYER";
		
	}

}