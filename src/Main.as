package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import net.flashpunk.Engine;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	
	/**
	 * ...
	 * @author galman33
	 */
	public class Main extends Engine 
	{
		
		public static const resHeight:int = 1280;
		public static const resWidth:int = 720;
		
		[Embed(source = "res/sounds/Music.mp3")]
	
		
		private static const BGMusic :Class;
		private static var BGMusicSfx :Sfx = new Sfx(BGMusic);
		
		public function Main():void 
		{
			super(resHeight, resWidth);
		}
		
		override public function init():void 
		{
			super.init();
			FP.world = new MenuWorld();
			BGMusicSfx.loop();
		}
		
		override public function update():void 
		{
			super.update();
			if (Input.pressed(Key.M))
				BGMusicSfx.volume = BGMusicSfx.volume == 1 ? 0 : 1;
		}
		
		public static function getVolume() :Number
		{
			return BGMusicSfx.volume;
		}
		
	}
	
}