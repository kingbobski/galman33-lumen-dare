package  
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	
	/**
	 * ...
	 * @author galman33
	 */
	public class EndTorch extends GameEntity 
	{
		
		[Embed(source = "res/EndTorch.png")]
		private static const EndTorchGraphic :Class;
		
		public function EndTorch(x :Number, y :Number) 
		{
			super();
			this.x = x;
			this.y = y;
			this.graphic = new Spritemap(ScaledBitmap.getScaledBitmap(EndTorchGraphic), 5 * GameWorld.SCALE, 10 * GameWorld.SCALE);
			(this.graphic as Spritemap).randFrame();
			graphic.x = -3 * GameWorld.SCALE;
			graphic.y = -10 * GameWorld.SCALE;
			FP.alarm(Math.random() * 0.75, animDone);
			type = Types.TYPE_END_TORCH;
			setHitbox(6 * GameWorld.SCALE, 4 * GameWorld.SCALE, 3 * GameWorld.SCALE, 2 * GameWorld.SCALE);
		}
		
		private function animDone():void 
		{
			(this.graphic as Spritemap).randFrame();
			FP.alarm(Math.random() * 0.75, animDone);
		}
		
	}

}