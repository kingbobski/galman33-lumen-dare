package  
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Emitter;
	import net.flashpunk.graphics.Particle;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.Tween;
	import net.flashpunk.Tweener;
	import net.flashpunk.tweens.misc.VarTween;
	import net.flashpunk.tweens.motion.LinearMotion;
	import net.flashpunk.utils.Ease;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	
	/**
	 * ...
	 * @author galman33
	 */
	public class EndPlayer extends GameEntity 
	{
		
		[Embed(source = "res/Player.png")]
		private static const PlayerGraphics :Class;
		
		private static const ANIMATION_MOVE_DOWN :String = "ANIMATION_MOVE_DOWN";
		private static const ANIMATION_MOVE_RIGHT :String = "ANIMATION_MOVE_RIGHT";
		private static const ANIMATION_MOVE_UP :String = "ANIMATION_MOVE_UP";
		private static const ANIMATION_MOVE_LEFT :String = "ANIMATION_MOVE_LEFT";
		private static const ANIMATION_WIN :String = "ANIMATION_WIN";
		
		private static const ANIMATION_LOOK_AT_PLAYER :String = "ANIMATION_LOOK_AT_PLAYER";
		private static const ANIMATION_LOOK_UP :String = "ANIMATION_LOOK_UP";
		private static const ANIMATION_ROTATE :String = "ANIMATION_ROTATE";
		
		private static const MOVE_ANIM_FPS :int = 5;
		
		private var spritemap :Spritemap;
		
		private var moveTween :LinearMotion;
		
		private var doneLevel :Boolean;
		
		private var state :int = 0;
		private var subState :int = 0;
		
		public function EndPlayer() 
		{
			super();
			graphic = spritemap = new Spritemap(ScaledBitmap.getScaledBitmap(PlayerGraphics), 10 * GameWorld.SCALE, 10 * GameWorld.SCALE, endAnim);
			
			spritemap.originX = 5 * GameWorld.SCALE;
			spritemap.originY = 8 * GameWorld.SCALE;
			
			spritemap.add(ANIMATION_MOVE_DOWN, [0, 1], MOVE_ANIM_FPS);
			spritemap.add(ANIMATION_MOVE_RIGHT, [2, 3], MOVE_ANIM_FPS);
			spritemap.add(ANIMATION_MOVE_UP, [4, 5], MOVE_ANIM_FPS);
			spritemap.add(ANIMATION_MOVE_LEFT, [6, 7], MOVE_ANIM_FPS);
			spritemap.add(ANIMATION_WIN, [2, 3, 8, 9, 6, 7, 4, 5], MOVE_ANIM_FPS);
			spritemap.add(ANIMATION_LOOK_AT_PLAYER, [1], 0.75);
			spritemap.add(ANIMATION_LOOK_UP, [1, 10, 10 , 10, 10, 1, 1, 9, 9, 9, 9], 1);
			spritemap.add(ANIMATION_ROTATE, [3, 5, 7, 9], 4);
			spritemap.play(ANIMATION_MOVE_DOWN);
			
			moveTween = addTween(new LinearMotion(doneMove)) as LinearMotion;
			
			doneLevel = false;
			
			setHitbox(5 * GameWorld.SCALE, 3 * GameWorld.SCALE, 2 * GameWorld.SCALE, GameWorld.SCALE);
			type = Types.TYPE_PLAYER;
			
			y = 350;
			x = -200;
			move(1, 0);
		}
		
		private function move(xAmount :int, yAmount :int):void 
		{
			if (!moveTween.active)
			{
				if (yAmount != 0)
				{
					if (yAmount > 0)
						spritemap.play(ANIMATION_MOVE_DOWN);
					else if(yAmount < 0)
						spritemap.play(ANIMATION_MOVE_UP);
				}
				else
				{
					if(xAmount > 0)
						spritemap.play(ANIMATION_MOVE_RIGHT);
					else if(xAmount < 0)
						spritemap.play(ANIMATION_MOVE_LEFT);
					
					if (xAmount > 0 || x != 58 * GameWorld.SCALE)
					{	
						moveTween.setMotion(x, y, x + xAmount * Level.TILE_WIDTH * GameWorld.SCALE, y + yAmount * Level.TILE_HEIGHT * GameWorld.SCALE, 0.3, Ease.quadInOut);
						moveTween.object = this;
						moveTween.start();
					}
				}
			}
		}
		
		private function doneMove():void 
		{
			switch(state)
			{
				case 0:
					subState++;
					if (subState < 12)
						move(1, 0);
					else
					{
						state++;
						subState = 0;
						spritemap.play(ANIMATION_MOVE_DOWN);
					}
					break;
					
			}
		}
		
		private function endAnim():void 
		{
			switch(state)
			{
				case 1:
					subState++;
					if (subState < 5)
					
						spritemap.play(ANIMATION_MOVE_DOWN);
					else
					{
						state++;
						subState = 0;
						spritemap.play(ANIMATION_LOOK_AT_PLAYER);
						Player.winSfx.play(0.25 * Main.getVolume());
					}
					break;
				case 2:
					subState++;
					if (subState == 1)
						spritemap.play(ANIMATION_LOOK_UP);
					else if (subState == 2)
					{
						spritemap.play(ANIMATION_ROTATE);
						var tween :VarTween = new VarTween(done);
						tween.tween(this, "y", -5, 7, Ease.expoIn);
						addTween(tween);
					}
					break;
			}
		}
		
		private function done():void 
		{
			(world as EndWorld).done();
		}
		
	}

}