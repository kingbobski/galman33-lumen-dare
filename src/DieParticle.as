package  
{
	import net.flashpunk.FP;
	/**
	 * ...
	 * @author galman33
	 */
	public class DieParticle 
	{
	
		private static const SPEED_BASE :Number = 50;
		private static const SPEED_RANGE :Number = 30;
		
		private static const VEL_A_BASE :Number = 0;
		private static const VEL_A_RANGE :Number = 90;
		
		private static const VEL_DRAG :Number = 50;
		private static const VEL_A_DRAG :Number = 20;
		
		public var x :Number;
		public var y :Number;
		public var angle :Number;
		public var color :Number;
		
		private var vel :Number;
		private var velAngle :Number;
		private var velA :Number;
		
		public function DieParticle(x :Number, y :Number) 
		{
			this.x = x;
			this.y = y;
			velAngle = Math.random() * Math.PI * 2;
			vel = (Math.random() * SPEED_RANGE * GameWorld.SCALE + SPEED_BASE);
			velA = Math.random() * VEL_A_RANGE * GameWorld.SCALE + VEL_A_BASE;
			if (Math.floor(Math.random() * 2) == 0)
				velA *= -1;
			angle = Math.random() * 360;
			this.color = FP.colorLerp(0x4DFDFC, 0x023ADA, Math.random());
		}
		
		public function update(elapsed :Number) :void
		{
			x += vel * Math.sin(velAngle) * elapsed;
			y += vel * Math.cos(velAngle) * elapsed;
			angle += velA * elapsed;
			vel = FP.approach(vel, 0, VEL_DRAG * GameWorld.SCALE * FP.elapsed);
			velA = FP.approach(velA, 0, VEL_A_DRAG * GameWorld.SCALE * FP.elapsed);
		}
		
	}

}