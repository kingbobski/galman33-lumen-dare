package  
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.utils.Draw;
	
	/**
	 * ...
	 * @author galman33
	 */
	public class DieParticles extends Entity 
	{
		
		[Embed(source = "res/Particle.png")]
		private static const ParticleGraphic :Class;
		
		private var particleImage :Image;
		
		private var particles :Vector.<DieParticle>;
		
		public function DieParticles(x :Number, y :Number) 
		{
			super()
			particleImage = new Image(ScaledBitmap.getScaledBitmap(ParticleGraphic));
			particleImage.centerOrigin();
			particles = new Vector.<DieParticle>();
			emitParticles(x, y);
			layer = FP.height;
		}
		
		override public function update():void 
		{
			super.update();
			for (var i :int = 0; i < particles.length; i++)
				particles[i].update(FP.elapsed);
		}
		
		override public function render():void 
		{
			for (var i :int = 0; i < particles.length; i++)
			{
				var particle :DieParticle = particles[i];
				particleImage.x = particle.x;
				particleImage.y = particle.y;
				particleImage.angle = particle.angle;
				particleImage.color = particle.color;
				Draw.graphic(particleImage);
			}
			
		}
		
		private function emitParticles(x :Number, y :Number) :void
		{
			for (var i :int = 40; i >= 0; i--)
				particles.push(new DieParticle(x, y));
		}
		
	}

}