package  
{
	import flash.sampler.NewObjectSample;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.Tween;
	import net.flashpunk.tweens.misc.NumTween;
	/**
	 * ...
	 * @author galman33
	 */
	public class Bridge extends Entity
	{
		
		[Embed(source = "res/Bridges.png")]
		private static const BridgesGraphics :Class;
		
		private var spritemap :Spritemap 
		
		private var passable :Boolean;
		
		private var alphaTween :NumTween;
		
		private var bridgeType :String;
		
		public function Bridge(x :int, y :int, bridgeType :String, onStart :String) 
		{
			super(x, y);
			
			spritemap = new Spritemap(ScaledBitmap.getScaledBitmap(BridgesGraphics), 12 * GameWorld.SCALE, 10 * GameWorld.SCALE);
			spritemap.add("Green", [0]);
			spritemap.add("Blue", [1]);
			spritemap.add("Red", [2]);
			spritemap.play(bridgeType);
			graphic = spritemap;
			type = Types.TYPE_BRIDGE;
			layer = Level.LAYER - 1;
			passable = onStart == "True";
			spritemap.alpha = onStart == "True" ? 1 : 0.25;
			
			setHitbox(8 * GameWorld.SCALE, 8 * GameWorld.SCALE, -2 * GameWorld.SCALE, -2 * GameWorld.SCALE);
			
			alphaTween = new NumTween();
			addTween(alphaTween);
			alphaTween.value = spritemap.alpha;
		}
		
		public function toggleIfType(type :String) :void
		{
			if (spritemap.currentAnim == type)
				togglePassable();
		}
		
		public function togglePassable() :void
		{
			passable = !passable;
			alphaTween.tween(passable ? 0.25 : 1, passable ? 1 : 0.25, 0.5);
			if(passable)
				alphaTween.percent = (spritemap.alpha - 0.25) / 0.75;
			else
				alphaTween.percent = 1 - (spritemap.alpha - 0.25) / 0.75;
			alphaTween.start();
		}
		
		override public function update():void 
		{
			super.update();
			
			spritemap.alpha = alphaTween.value * Level.lightness;			
		}
		
		public function isPassable() :Boolean
		{
			return passable;
		}
		
	}

}