package  
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Spritemap;
	
	/**
	 * ...
	 * @author galman33
	 */
	public class Button extends Entity 
	{
		
		[Embed(source = "res/Buttons.png")]
		private static const ButtonsGraphics :Class;
		
		private var spritemap :Spritemap;
		
		public function Button(x :int, y :int, buttonType :String) 
		{
			super();
			this.x = x;
			this.y = y;
			spritemap = new Spritemap(ScaledBitmap.getScaledBitmap(ButtonsGraphics), 11 * GameWorld.SCALE, 9 * GameWorld.SCALE);
			spritemap.add("Green", [0]);
			spritemap.add("Blue", [1]);
			spritemap.add("Red", [2]);
			spritemap.play(buttonType);
			graphic = spritemap;
			type = Types.TYPE_BUTTON;
			layer = Level.LAYER - 1;
			setHitbox(GameWorld.SCALE * 8, GameWorld.SCALE * 6, -GameWorld.SCALE, -GameWorld.SCALE);
		}
		
		public function push() :void
		{
			var brigdes :Vector.<Bridge> = new Vector.<Bridge>();
			FP.world.getClass(Bridge, brigdes);
			for each(var bridge :Bridge in brigdes)
				bridge.toggleIfType(spritemap.currentAnim);
		}
		
		override public function update():void 
		{
			super.update();
			spritemap.alpha = Level.lightness;
		}
		
	}

}