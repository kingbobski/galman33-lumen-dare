package  
{
	import adobe.utils.CustomActions;
	import flash.geom.Rectangle;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Backdrop;
	import net.flashpunk.graphics.Canvas;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.graphics.Tilemap;
	import net.flashpunk.tweens.misc.VarTween;
	import net.flashpunk.utils.Ease;
	import net.flashpunk.World;
	
	/**
	 * ...
	 * @author galman33
	 */
	public class MenuWorld extends World 
	{
		
		[Embed(source = "res/Background.png")]
		private static const BackgroundGraphic :Class;
		
		[Embed(source = "res/Menu.png")]
		private static const MenuGraphic :Class;
		
		private var menuPlayer :MenuPlayer;
		
		public function MenuWorld() 
		{
			super();
			
			var background :Backdrop = new Backdrop(ScaledBitmap.getScaledBitmap(BackgroundGraphic));
			addGraphic(background);
			
			var menuImage :Image = new Image(MenuGraphic);
			menuImage.scale = GameWorld.SCALE;
			addGraphic(menuImage);
			
			addGraphic(new MenuFire(113, 18)); 
			
			menuPlayer = new MenuPlayer();
			menuPlayer.x = 58 * GameWorld.SCALE;
			menuPlayer.y = 68 * GameWorld.SCALE;
			add(menuPlayer);
			
			add(new EndTorch(102.5 * GameWorld.SCALE, 68 * GameWorld.SCALE));
			
			var moveRightTxt :Text = new Text("Move Right to Start");
			moveRightTxt.align = "center";
			moveRightTxt.width = FP.width;
			moveRightTxt.y = 322;
			addGraphic(moveRightTxt);
			
			var creditTxt :Text = new Text("Made by @galman33 in 48 hours for Ludum Dare 27");
			creditTxt.y = 460;
			creditTxt.x = 5;
			creditTxt.color = 0x4DBCFD;
			addGraphic(creditTxt);
		}
		
		
		public function playerWon():void 
		{
			var transScreen :Canvas = new Canvas(FP.width, FP.height);
			transScreen.fill(new Rectangle(0, 0, FP.width, FP.height));
			transScreen.alpha = 0;
			addGraphic(transScreen, -9999);
			
			var transScreenTween :VarTween = new VarTween(nextLevel);
			transScreenTween.tween(transScreen, "alpha", 1, 0.5, Ease.cubeOut);
			transScreenTween.delay = 2;
			addTween(transScreenTween);		
		}
		
		private function nextLevel():void 
		{
			FP.world = new GameWorld(0);
		}
		
		
		
	}

}