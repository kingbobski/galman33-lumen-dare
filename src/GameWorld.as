package  
{
	import flash.geom.Rectangle;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Backdrop;
	import net.flashpunk.graphics.Canvas;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.tweens.misc.VarTween;
	import net.flashpunk.utils.Ease;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.World;
	
	/**
	 * ...
	 * @author galman33
	 */
	public class GameWorld extends World 
	{
		
		public static const SCALE :int = 4;
		private static const MAX_LIGHT_TIME :Number = 10;
		
		[Embed(source = "res/Background.png")]
		private static const BackgroundGraphic :Class;
		
		private var gui :Gui;
		private var player :Player;
		private var level :Level;
		
		private var lightLeftTime :Number;
		private var lighted :Boolean;
		
		private var playerDead :Boolean;
		private var switchingLevel :Boolean;
		
		private var transScreen :Canvas;
		
		private var levelNum:int;
		
		public function GameWorld(levelNum :int) 
		{
			super();
			this.levelNum = levelNum;
		}
		
		override public function begin():void 
		{
			super.begin();
			
			var background :Backdrop = new Backdrop(ScaledBitmap.getScaledBitmap(BackgroundGraphic));
			addGraphic(background, 9999);
			
			gui = new Gui();
			add(gui);
			player = new Player();
			add(player);
			level = new Level(levelNum);
			add(level);
			
			gui.setTitleText(level.title);
			
			lightLeftTime = 10;
			turnOffLights();
			
			playerDead = false;
			switchingLevel = false;
			
			transScreen = new Canvas(FP.width, FP.height);
			transScreen.fill(new Rectangle(0, 0, transScreen.width, transScreen.height));
			addGraphic(transScreen, -9999);
			var transScreenTween :VarTween = new VarTween();
			transScreenTween.tween(transScreen, "alpha", 0, 0.5, Ease.cubeIn);
			transScreen.scrollX = transScreen.scrollY = 0;
			addTween(transScreenTween);
		}
		
		override public function update():void 
		{
			super.update();
			if (lighted && lightLeftTime > 0)
			{
				lightLeftTime -= FP.elapsed;
				if (lightLeftTime <= 0)
				{
					lightLeftTime = 0;
					turnOffLights();
				}
			}
			
			if (Input.pressed(Key.SPACE))
			{
				if (playerDead)
				{
					if (!switchingLevel)
					{
						switchingLevel = true;
						
						var transScreenTween :VarTween = new VarTween(restartLevel);
						transScreenTween.tween(transScreen, "alpha", 1, 0.5, Ease.cubeOut);
						addTween(transScreenTween);
					}
				}
				else
				{
					if (lighted)
						turnOffLights();
					else
						turnOnLights();
				}
			}
			FP.camera.x = Math.floor(FP.lerp(FP.camera.x, FP.clamp(player.x - FP.halfWidth, 0, level.getMazeWidth() - FP.width), 1 * FP.elapsed * SCALE));
			FP.camera.y = Math.floor(FP.lerp(FP.camera.y, FP.clamp(player.y - FP.halfHeight, 0, level.getMazeHeight() - FP.height), 1 * FP.elapsed * SCALE));
			
		}
		
		private function restartLevel():void 
		{
			FP.world = new GameWorld(levelNum);
		}
		
		private function turnOnLights():void 
		{
			if (lightLeftTime > 0)
			{
				lighted = true;
				level.lightOn();
			}
		}
		
		private function turnOffLights():void 
		{
			lighted = false;
			level.lightOff();
		}
		
		public function getLightLeftPercent() :Number
		{
			return lightLeftTime / MAX_LIGHT_TIME;
		}
		
		public function setPlayerPosition(x :int, y :int) :void
		{
			player.x = x;
			player.y = y;
		}
		
		public function playerDied():void 
		{
			add(new DieParticles(player.x, player.y));
			playerDead = true;
			turnOffLights();
			
			var restartTxt :Text = new Text("Oh no...\nPress Space");
			restartTxt.align = "center";
			restartTxt.size = 30;
			restartTxt.x = FP.width;
			restartTxt.y = (FP.height - restartTxt.height) / 2;
			restartTxt.color = 0xFF0080;
			restartTxt.scrollX = restartTxt.scrollY = 0;
			var restartTween :VarTween = new VarTween();
			restartTween.tween(restartTxt, "x", (FP.width - restartTxt.width) / 2, 2, Ease.bounceOut);
			addTween(restartTween);
			addGraphic(restartTxt, 0);
		}
		
		public function getLightLeft():Number 
		{
			return lightLeftTime;
		}
		
		public function playerWon():void 
		{
			switchingLevel = true;
			var transScreenTween :VarTween = new VarTween(nextLevel);
			transScreenTween.tween(transScreen, "alpha", 1, 0.5, Ease.cubeOut);
			transScreenTween.delay = 2;
			addTween(transScreenTween);
		}
		
		private function nextLevel():void 
		{
			if(levelNum + 1 < Levels.LEVELS.length)
				FP.world = new GameWorld(levelNum + 1);
			else
				FP.world = new EndWorld();
		}
		
	}

}