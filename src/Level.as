package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Canvas;
	import net.flashpunk.graphics.Tilemap;
	import net.flashpunk.tweens.misc.NumTween;
	import net.flashpunk.tweens.misc.VarTween;
	
	/**
	 * ...
	 * @author galman33
	 */
	public class Level extends Entity 
	{
		
		public static const TILE_WIDTH :int = 11;
		public static const TILE_HEIGHT :int = 9;
		
		public static const LAYER :int = 9999;
		
		[Embed(source = "res/Tileset.png")]
		private static const TILESET_GRAPHIC :Class;
		
		[Embed(source = "res/EndLight.png")]
		private var EndLightGraphic :Class;
		
		public static var lightness :Number;
		
		private var mazeTilemap :Tilemap;
		
		private var mazeLightTween :VarTween;
		
		public var title :String;
		
		private var darknessCanvas :Canvas;
		
		public function Level(level :int) 
		{
			super();
			
			var levelData :ByteArray = new Levels.LEVELS[level];
			var levelXml :XML = new XML(levelData.readUTFBytes(levelData.length));
			
			mazeTilemap = new Tilemap(ScaledBitmap.getScaledBitmap(TILESET_GRAPHIC), ((int) (levelXml.@width)) * GameWorld.SCALE,
				((int) (levelXml.@height)) * GameWorld.SCALE, TILE_WIDTH * GameWorld.SCALE, TILE_HEIGHT * GameWorld.SCALE);
			mazeTilemap.loadFromString(levelXml.Maze, "");
			
			for (var row :int = 0; row < mazeTilemap.rows; row++)
				for (var column :int = 0; column < mazeTilemap.columns; column++)
				{
					if (mazeTilemap.getTile(column, row) == 0)
					{
						if (row + 1 < mazeTilemap.rows && mazeTilemap.getTile(column, row + 1) == 1)
						{
							if (column + 1 < mazeTilemap.columns && mazeTilemap.getTile(column + 1, row) == 1)
								mazeTilemap.setTile(column, row, 4);
							else
								mazeTilemap.setTile(column, row, 3);
						}
						else if (column + 1 < mazeTilemap.columns && mazeTilemap.getTile(column + 1, row) == 1)
							mazeTilemap.setTile(column, row, 2);
					}
				}
			
			(FP.world as GameWorld).setPlayerPosition(((int) (levelXml.Entities.Player.@x) + 5) * GameWorld.SCALE, ((int) (levelXml.Entities.Player.@y) + 5) * GameWorld.SCALE);
			(FP.world as GameWorld).add(new EndTorch(((int) (levelXml.Entities.EndTorch.@x) + 5.5) * GameWorld.SCALE, ((int) (levelXml.Entities.EndTorch.@y) + 5) * GameWorld.SCALE));
			
			var test :Canvas = new Canvas(mazeTilemap.width, mazeTilemap.height);
			var testBitmap :BitmapData = new BitmapData(mazeTilemap.width, mazeTilemap.height, true, 0);
			mazeTilemap.render(testBitmap, new Point(), new Point());
			var lighBitmapdata :BitmapData = ScaledBitmap.getScaledBitmap(EndLightGraphic);
			var mat :Matrix = new Matrix();
			mat.translate(GameWorld.SCALE * (levelXml.Entities.EndTorch.@x) - 2, GameWorld.SCALE * (levelXml.Entities.EndTorch.@y) - 5);
			var test2 :BitmapData = new BitmapData(mazeTilemap.width, mazeTilemap.height, true, 0);
//			test2.draw(lighBitmapdata, mat);
			testBitmap.merge(test2, test2.rect, new Point(), 0x00, 0x00, 0x00, 0xFF);
//			test.draw(0, 0, testBitmap);
			
			addGraphic(mazeTilemap);
			layer = LAYER;
			
			mazeLightTween = new VarTween();
			addTween(mazeLightTween);
					
			FP.world.addGraphic(test, LAYER - 3);
			
			FP.world.addMask(mazeTilemap.createGrid([1]), Types.TYPE_MAZE);
			
			/*darknessCanvas = new Canvas(FP.width, FP.height);
			darknessCanvas.fill(new Rectangle(0, 0, FP.width, FP.height), FP.screen.color);
			darknessCanvas.scrollX = darknessCanvas.scrollY = 0;
			darknessCanvas.blend = BlendMode.DIFFERENCE;
			FP.world.addGraphic(darknessCanvas, LAYER - 2);*/
			
			lightness = 0;
			
			title = levelXml.@LevelText;
			
			for each (var buttonXml :XML in levelXml.Entities.Button)
				(FP.world as GameWorld).add(new Button((int)(buttonXml.@x) * GameWorld.SCALE, (int)(buttonXml.@y) * GameWorld.SCALE, buttonXml.@Type));
			for each (var bridgeXml :XML in levelXml.Entities.Bridge)
				(FP.world as GameWorld).add(new Bridge(((int)(bridgeXml.@x) - 1) * GameWorld.SCALE, ((int)(bridgeXml.@y) - 1) * GameWorld.SCALE, bridgeXml.@Type, bridgeXml.@OnStart));
		}
		
		override public function update():void 
		{
			super.update();
			mazeTilemap.alpha = lightness;
		}
		
		public function lightOn():void 
		{
			var cur :Number = lightness;
			lightness = 0;
			mazeLightTween.tween(Level, "lightness", 1, 0.3);
			mazeLightTween.start();
			lightness = cur;
			mazeLightTween.percent = cur;
		}
		
		public function lightOff():void 
		{
			var cur :Number = lightness;
			lightness = 1
			mazeLightTween.tween(Level, "lightness", 0, 0.3);
			mazeLightTween.start();
			lightness = cur;
			mazeLightTween.percent = 1 - cur;
		}
		
		public function getMazeWidth() :Number
		{
			return mazeTilemap.width;
		}
		
		public function getMazeHeight() :Number
		{
			return mazeTilemap.height;
		}
		
	}

}