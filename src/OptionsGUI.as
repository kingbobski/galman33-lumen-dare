package  
{
	
	import flash.geom.Rectangle;
	import flash.utils.getTimer;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Canvas;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.graphics.Text;
	
	/**
	 * ...
	 * @author Kingbobski
	 */
	public class OptionsGUI 
	{
		
		public function OptionsGUI() 
		{
			super();
			layer = 0;
			lightSprite = new Spritemap(LightGraphic, 11, 18);
			lightSprite.scale = 2;
			lightSprite.y = 10;
			lightSprite.frame = 0;
			
			var lightBarImage :Image = new Image(LightBarGraphic);
			lightBarImage.scale = 4;
			lightBarImage.x = (FP.width - lightBarImage.scaledWidth) / 2;
			lightBarImage.y = 10;
			
			lightBarFillCanvas = new Canvas(lightBarImage.scaledWidth - 6 * GameWorld.SCALE, lightBarImage.scaledHeight - 6 * GameWorld.SCALE);
			lightBarFillCanvas.x = lightBarImage.x + 3 * GameWorld.SCALE;
			lightBarFillCanvas.y = lightBarImage.y  + 3 * GameWorld.SCALE;
			
			lightSprite.x = lightBarImage.x - lightSprite.scaledWidth - 10;
			
			timeLeft = new Text("10.0");
			timeLeft.size = 16;
			timeLeft.color = 0xFF0080;
			timeLeft.x = (FP.width - timeLeft.scaledWidth) / 2;
			timeLeft.y = 21;
			
			titleText = new Text("");
			titleText.width = FP.width;
			titleText.align = "center";
			titleText.y = 50;
			
			/*
			addGraphic(lightSprite);
			addGraphic(lightBarFillCanvas);
			addGraphic(lightBarImage);
			addGraphic(timeLeft);
			addGraphic(titleText);
			*/
			graphic.scrollX = graphic.scrollY = 0;
			
			
		}
		
	}

}