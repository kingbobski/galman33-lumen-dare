package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	import net.flashpunk.FP;
	/**
	 * ...
	 * @author galman33
	 */
	public class ScaledBitmap 
	{
		
		public static function getScaledBitmap(source:*, scale :Number = 0) :BitmapData
		{
			if (scale == 0)
				scale = GameWorld.SCALE;
			var orig :BitmapData = FP.getBitmap(source);
			var newBitmap :BitmapData = new BitmapData(orig.width * scale, orig.height * scale, orig.transparent,0x000000);
			var mat :Matrix = new Matrix();
			mat.scale(scale, scale);
			newBitmap.draw(orig, mat);
			return newBitmap;
		}
		
	}

}