package  
{
	import flash.geom.Rectangle;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Backdrop;
	import net.flashpunk.graphics.Canvas;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.tweens.misc.MultiVarTween;
	import net.flashpunk.tweens.misc.VarTween;
	import net.flashpunk.utils.Ease;
	import net.flashpunk.World;
	
	/**
	 * ...
	 * @author galman33
	 */
	public class EndWorld extends World 
	{
		
		[Embed(source = "res/Background.png")]
		private static const BackgroundGraphic :Class;
		
		[Embed(source = "res/End1.png")]
		private static const End1Graphic :Class;
		
		[Embed(source = "res/End2.png")]
		private static const End2Graphic :Class;
		
		public function EndWorld() 
		{
			super();
			
			var background :Backdrop = new Backdrop(ScaledBitmap.getScaledBitmap(BackgroundGraphic));
			addGraphic(background);
			
			add(new EndPlayer());
			
			var transScreen :Canvas = new Canvas(FP.width, FP.height);
			transScreen.fill(new Rectangle(0, 0, transScreen.width, transScreen.height));
			addGraphic(transScreen, -9999);
			var transScreenTween :VarTween = new VarTween();
			transScreenTween.tween(transScreen, "alpha", 0, 0.5, Ease.cubeIn);
			transScreen.scrollX = transScreen.scrollY = 0;
			addTween(transScreenTween);
		}
		
		public function done():void 
		{
			var end1Image :Image = new Image(End1Graphic);
			end1Image.scale = GameWorld.SCALE;
			end1Image.x = (FP.width - end1Image.scaledWidth) / 2;
			end1Image.y = (FP.height - end1Image.scaledHeight) / 2;
			end1Image.alpha = 0;
			var tween :VarTween = new VarTween(done2);
			tween.delay = 2;
			tween.tween(end1Image, "alpha", 1, 3);
			addTween(tween);
			addGraphic(end1Image);
		}
		
		private function done2():void 
		{
			var end2Image :Image = new Image(End2Graphic);
			end2Image.scale = GameWorld.SCALE;
			end2Image.alpha = 0;
			end2Image.x = 450;
			end2Image.y = 230;
			var tween :VarTween = new VarTween();
			tween.tween(end2Image, "alpha", 1, 0.5);
			tween.delay = 2;
			addTween(tween);
			addGraphic(end2Image);
			
			var creditTxt :Text = new Text("Made by @galman33 in 48 hours for Ludum Dare 27");
			creditTxt.y = 460;
			creditTxt.x = 5;
			creditTxt.color = 0x4DBCFD;
			creditTxt.alpha = 0;
			addGraphic(creditTxt);
			
			var tween2 :VarTween = new VarTween();
			tween2.tween(creditTxt, "alpha", 1, 0.5);
			tween2.delay = 2;
			addTween(tween2);
		}
		
	}

}