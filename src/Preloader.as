package
{
   import flash.display.DisplayObject;
   import flash.display.MovieClip;
   import flash.display.MovieClip;
   import flash.events.Event;
   import flash.utils.getDefinitionByName;
   
   public class Preloader extends MovieClip
   {
      
      public function Preloader() 
      {
         addEventListener(Event.ENTER_FRAME, progress);
         // show loader
      }
      
      private function progress(e:Event):void 
      {
		  graphics.clear();
		  graphics.beginFill(0x0000FF);
		  graphics.drawRect(0, 0, loaderInfo.bytesLoaded / loaderInfo.bytesTotal * Main.resHeight , Main.resWidth );
		  
         // update loader
         if (currentFrame == totalFrames)
         {
            removeEventListener(Event.ENTER_FRAME, progress);
            startup();
         }
      }
      
      private function startup():void 
      {
         // hide loader
         stop();
         // instanciate Main class
         var mainClass:Class = getDefinitionByName("Main") as Class;
         addChild(new mainClass() as DisplayObject);
      }
      
   }
   
}