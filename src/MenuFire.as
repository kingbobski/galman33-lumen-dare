package  
{
	import flash.display.Sprite;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Spritemap;
	
	/**
	 * ...
	 * @author galman33
	 */
	public class MenuFire extends Spritemap
	{
		
		[Embed(source = "res/MenuTorch.png")]
		private static const MenuFireGraphic :Class;
		
		public function MenuFire(x :int, y :int) 
		{
			super(ScaledBitmap.getScaledBitmap(MenuFireGraphic), 4 * GameWorld.SCALE, 6 * GameWorld.SCALE);
			this.x = x * GameWorld.SCALE;
			this.y = y * GameWorld.SCALE;
			fireAnimDone();
		}
		
		private function fireAnimDone():void 
		{
			randFrame();
			FP.alarm(Math.random() * 0.75 + 0.25, fireAnimDone);
		}
		
	}

}