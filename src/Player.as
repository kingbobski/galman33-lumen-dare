package  
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Emitter;
	import net.flashpunk.graphics.Particle;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.Sfx;
	import net.flashpunk.Tween;
	import net.flashpunk.Tweener;
	import net.flashpunk.tweens.misc.VarTween;
	import net.flashpunk.tweens.motion.LinearMotion;
	import net.flashpunk.utils.Ease;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	
	/**
	 * ...
	 * @author galman33
	 */
	public class Player extends GameEntity 
	{
		
		[Embed(source = "res/Player.png")]
		private static const PlayerGraphics :Class;
		
		[Embed(source = "res/sounds/Win.mp3")]
		private static const WindSnd :Class;
		public static var winSfx :Sfx = new Sfx(WindSnd);
		
		[Embed(source = "res/sounds/Die.mp3")]
		private static const DieSnd :Class;
		private static var dieSfx :Sfx = new Sfx(DieSnd);
		
		[Embed(source = "res/sounds/Button.mp3")]
		private static const ButtonSnd :Class;
		private static var buttonSfx :Sfx = new Sfx(ButtonSnd);
		
		private static const ANIMATION_MOVE_DOWN :String = "ANIMATION_MOVE_DOWN";
		private static const ANIMATION_MOVE_RIGHT :String = "ANIMATION_MOVE_RIGHT";
		private static const ANIMATION_MOVE_UP :String = "ANIMATION_MOVE_UP";
		private static const ANIMATION_MOVE_LEFT :String = "ANIMATION_MOVE_LEFT";
		private static const ANIMATION_WIN :String = "ANIMATION_WIN";
		
		private static const MOVE_ANIM_FPS :int = 5;
		
		private var spritemap :Spritemap;
		
		private var moveTween :LinearMotion;
		
		private var doneLevel :Boolean;
		
		public function Player() 
		{
			super();
			graphic = spritemap = new Spritemap(ScaledBitmap.getScaledBitmap(PlayerGraphics), 10 * GameWorld.SCALE, 10 * GameWorld.SCALE);
			
			spritemap.originX = 5 * GameWorld.SCALE;
			spritemap.originY = 8 * GameWorld.SCALE;
			
			spritemap.add(ANIMATION_MOVE_DOWN, [0, 1], MOVE_ANIM_FPS);
			spritemap.add(ANIMATION_MOVE_RIGHT, [2, 3], MOVE_ANIM_FPS);
			spritemap.add(ANIMATION_MOVE_UP, [4, 5], MOVE_ANIM_FPS);
			spritemap.add(ANIMATION_MOVE_LEFT, [6, 7], MOVE_ANIM_FPS);
			spritemap.add(ANIMATION_WIN, [2, 3, 8, 9, 6, 7, 4, 5], MOVE_ANIM_FPS);
			spritemap.play(ANIMATION_MOVE_DOWN);
			
			moveTween = addTween(new LinearMotion(doneMove)) as LinearMotion;
			
			doneLevel = false;
			
			setHitbox(5 * GameWorld.SCALE, 3 * GameWorld.SCALE, 2 * GameWorld.SCALE, GameWorld.SCALE);
			type = Types.TYPE_PLAYER;
		}
		
		override public function update():void 
		{
			super.update();
			input();
		}
		
		private function input():void 
		{
			if (!doneLevel)
			{
				if (Input.check(Key.RIGHT))
					move(1, 0);
				else if (Input.check(Key.LEFT))
					move(-1, 0);
				else if (Input.check(Key.UP))
					move(0, -1);
				else if (Input.check(Key.DOWN))
					move(0, 1);
			}
		}
		
		private function move(xAmount :int, yAmount :int):void 
		{
			if (!moveTween.active)
			{
				if (yAmount > 0)
					spritemap.play(ANIMATION_MOVE_DOWN);
				else if(yAmount < 0)
					spritemap.play(ANIMATION_MOVE_UP);
				else if(xAmount > 0)
					spritemap.play(ANIMATION_MOVE_RIGHT);
				else if(xAmount < 0)
					spritemap.play(ANIMATION_MOVE_LEFT);
				moveTween.setMotion(x, y, x + xAmount * Level.TILE_WIDTH * GameWorld.SCALE, y + yAmount * Level.TILE_HEIGHT * GameWorld.SCALE, 0.3, Ease.quadInOut);
				moveTween.object = this;
				moveTween.start();
			}
		}
		
		private function doneMove():void 
		{
			if (collide(Types.TYPE_MAZE, x, y) == null)
			{
				var standingBridge :Bridge = collide(Types.TYPE_BRIDGE, x, y) as Bridge;
				if(!(standingBridge != null && standingBridge.isPassable()))
					die();
			}
			if (collide(Types.TYPE_END_TORCH, x, y) != null)
				win();
			var standingButton :Button = collide(Types.TYPE_BUTTON, x, y) as Button;
			if (standingButton != null)
			{
				standingButton.push();
				buttonSfx.play(0.25 * Main.getVolume());
			}
		}
		
		private function win():void 
		{
			(world as GameWorld).playerWon();
			spritemap.play(ANIMATION_WIN);
			
			doneLevel = true;
			
			winSfx.play(0.25 * Main.getVolume());
		}
		
		private function die():void 
		{
			(world as GameWorld).playerDied();
			
			var alphaTween :VarTween = new VarTween();
			alphaTween.tween(spritemap, "alpha", 0, 0.5, Ease.expoOut);
			addTween(alphaTween);
			var scaleTween :VarTween = new VarTween();
			scaleTween.tween(spritemap, "scale", 0, 0.5, Ease.circIn);
			addTween(scaleTween);
			
			doneLevel = true;
			
			dieSfx.play(1.5 * Main.getVolume());
		}
		
	}

}